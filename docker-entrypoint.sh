#!/bin/bash
set -e

COMPOSER_DISCARD_CHANGES=true php composer.phar install -o --no-interaction
chown :www-data . -R

exec "$@"