LajiForm
=======================

Dev env
------------
1. Install php > 5.6 and oracle instanclient.
2. Fetch composer and run php composer.phar install. If there are any error follow the instructions to fix them
3. Copy to ./config/autoload/ all \*.local.php files from formtest.laji.fi or from other developer
4. Serve the files either throu webserver or using php build in server `php -S 0.0.0.0:8080 -t public/ public/index.php`
5. goto localhost:8080
