<?php

namespace LajiForm\Options;

use Zend\Log\Logger;
use Zend\Stdlib\AbstractOptions;

class LogOptions extends AbstractOptions
{
    /** @var  array */
    protected $file = './data/logs/%s.log';
    /** @var  string  */
    protected $level = Logger::WARN;
    /** @var string */
    protected $pattern = 'Y-m';

    /**
     * @return array
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param array $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param string $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    }
}