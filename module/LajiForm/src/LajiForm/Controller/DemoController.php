<?php

namespace LajiForm\Controller;

use LajiStoreClient\Service\LajiStoreClient;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DemoController extends AbstractActionController
{
    private $client;

    public function __construct(LajiStoreClient $client)
    {
        $this->client = $client;
    }

    public function indexAction()
    {
        $data = [];
        $data['forms'] = $this->getAllForms();
        return new ViewModel($data);
    }

    protected function getAllForms() {
        $results = $this->client->sendGet(LajiStoreClient::RESOURCE_FORMS, null, ['page_size' => 100, 'page' => 1]);
        if (!$results->isSuccess()) {
            $this->getResponse()->setStatusCode(500);
            return [];
        }
        $forms = [];
        foreach($results as $result) {
            $parts = explode('/', $result['id']);
            $id = array_pop($parts);
            $forms[$id] = isset($result['name']) ? $result['name'] : 'unnamed';
        }
        return $forms;
    }
}
