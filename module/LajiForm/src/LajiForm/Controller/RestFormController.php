<?php

namespace LajiForm\Controller;


use LajiForm\Service\FormClient;
use LajiStoreClient\Http\Result;
use LajiStoreClient\Service\LajiStoreClient;
use LForm\Converter\SchemaConverter;
use LForm\Converter\SpecConverter;
use LForm\Form\View\HelperConfig;
use LForm\Service\FormService;
use LForm\Template\Bootstrap;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Mvc\Exception;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\RequestInterface;
use Zend\View\HelperPluginManager;
use Zend\View\Model\JsonModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Renderer\RendererInterface;
use Zend\View\Resolver\TemplateMapResolver;

class RestFormController extends AbstractRestfulController
{
    /** @var FormClient */
    private $formClient;
    /** @var FormService */
    private $formService;

    private $renderer;

    public function __construct(FormClient $formClient, FormService $formService, RendererInterface $renderer)
    {
        $this->formClient = $formClient;
        $this->formService = $formService;
        $this->renderer = $renderer;
    }

    public function getList()
    {
        $this->formClient->setResultLang($this->getRequest()->getQuery('lang'));

        return new JsonModel($this->formClient->getAllForms());
    }

    public function get($id)
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $format  = $this->getFormat();
        $expand  = $request->getQuery('expand', 'true') === 'true' ? true : false;
        $this->formClient->setResultLang($request->getQuery('lang'));
        switch($format) {
            case 'html':
                //return 'lajifor.sj';
                return $this->getHtml($id);
                break;
            case 'schema':
                $instructions = $this->formClient->getForm($id);
                $hasData      = count($instructions) > 0;
                $converter    = new SchemaConverter();
                $data = $converter->convertFrom($instructions, $id);
                break;
            case 'spec':
                $instructions = $this->formClient->getForm($id);
                $hasData      = count($instructions) > 0;
                $converter    = new SpecConverter();
                $data = $converter->convertFrom($instructions, $id);
                break;
            default:
                $data = $this->formClient->getForm($id, $expand);
                $hasData      = count($data) > 0;
        }
        if (!$hasData) {
            $response = $this->getResponse();
            $response->setStatusCode(404);
            return $response;
        }
        return new JsonModel($data);
    }

    protected function getFormat() {
        /** @var Request $request */
        $request = $this->getRequest();
        $format = $request->getQuery('format', 'json-ld');
        if ($format !== null) {
            return $format;
        }
        /** @var \Zend\Http\Header\Accept $accept */
        $accept = $request->getHeader('Accept');
        if ($accept->match('application/json') || $accept->match('application/*-json')) {
            return 'json-ld';
        }
        if ($accept->match('text/html') || $accept->match('application/*-xml')) {
            return 'html';
        }
        if ($accept->match('application/schema-json')) {
            return 'schema';
        }
        return 'json-ld';

    }

    protected function getHtml($id, $form = null) {
        $mapResolver = new TemplateMapResolver();
        $view = $this->formService->getViewModel($id, new Bootstrap('./data', $mapResolver), $form);
        /** @var PhpRenderer $phpRenderer */
        $phpRenderer = $this->renderer;
        $phpRenderer->setHelperPluginManager(new HelperPluginManager(new HelperConfig()));
        $mapResolver->add('error/index', 'view/error/index.phtml');
        $phpRenderer->setResolver($mapResolver);
        return $view;
    }

    public function post($id, $data) {
        /** @var Request $request */
        $request = $this->getRequest();
        $format = $request->getQuery('format', 'json');
        $form = $this->formService->getForm($id);
        $form->setData($data);
        $isValid = $form->isValid();
        switch($format) {
            case'html':
                return $this->getHtml($id, $form);
                break;
            default:
                $inputFilter = $form->getInputFilter();
                $warningFilter = $form->getInputFilterWarning();
                $hasWarnings = !$warningFilter->setData($data)->isValid();
                return new JsonModel([
                    'isValid' => $isValid,
                    'hasWarnings' => $hasWarnings,
                    'errors' => $inputFilter->getMessages(),
                    'warnings' => $warningFilter->getMessages(),
                    'data' => $form->getData()
                ]);
        }
    }

    public function create($data)
    {
        $result = $this->formClient->saveForm($data);
        return $result->getResponse();
    }

    public function update($id, $data)
    {
        $result = $this->formClient->saveForm($data, $id);
        return $result->getResponse();
    }

    public function delete($id)
    {
        $result = $this->formClient->deleteForm($id);
        return $result->getResponse();
    }

    /**
     * Handle the request
     *
     * @todo   try-catch in "patch" for patchList should be removed in the future
     * @param  MvcEvent $e
     * @return mixed
     * @throws Exception\DomainException if no route matches in event or invalid HTTP method
     */
    public function onDispatch(MvcEvent $e)
    {
        $routeMatch = $e->getRouteMatch();
        if (! $routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException(
                'Missing route matches; unsure how to retrieve action');
        }

        /** @var Request $request */
        $request = $e->getRequest();

        // Was an "action" requested?
        $action  = $routeMatch->getParam('action', false);
        if ($action) {
            // Handle arbitrary methods, ending in Action
            $method = static::getMethodFromAction($action);
            if (! method_exists($this, $method)) {
                $method = 'notFoundAction';
            }
            $return = $this->$method();
            $e->setResult($return);
            return $return;
        }

        try {
            // RESTful methods
            $method = strtolower($request->getMethod());
            switch ($method) {
                // Custom HTTP methods (or custom overrides for standard methods)
                case (isset($this->customHttpMethodsMap[$method])):
                    $callable = $this->customHttpMethodsMap[$method];
                    $action = $method;
                    $return = call_user_func($callable, $e);
                    break;
                // DELETE
                case 'delete':
                    $id = $this->getIdentifier($routeMatch, $request);
                    $data = $this->processBodyContent($request);

                    if ($id !== false) {
                        $action = 'delete';
                        $return = $this->delete($id);
                        break;
                    }

                    $action = 'deleteList';
                    $return = $this->deleteList($data);
                    break;
                // GET
                case 'get':
                    $id = $this->getIdentifier($routeMatch, $request);

                    if ($id !== false) {
                        $action = 'get';
                        $return = $this->get($id);
                        break;
                    }

                    $action = 'getList';
                    $return = $this->getList();
                    break;
                // HEAD
                case 'head':
                    $id = $this->getIdentifier($routeMatch, $request);

                    if ($id === false) {
                        $id = null;
                    }

                    $action = 'head';
                    $headResult = $this->head($id);
                    $response = ($headResult instanceof Response) ? clone $headResult : $e->getResponse();
                    $response->setContent('');
                    $return = $response;
                    break;
                // OPTIONS
                case 'options':
                    $action = 'options';
                    $this->options();
                    $return = $e->getResponse();
                    break;
                // PATCH
                case 'patch':
                    $id = $this->getIdentifier($routeMatch, $request);
                    $data = $this->processBodyContent($request);

                    if ($id !== false) {
                        $action = 'patch';
                        $return = $this->patch($id, $data);
                        break;
                    }

                    $action = 'patchList';
                    $return = $this->patchList($data);

                    break;
                // POST
                case 'post':
                    $id     = $this->getIdentifier($routeMatch, $request);
                    $data   = $this->processPostData($request);

                    if ($id !== false) {
                        $action = 'post';
                        $return = $this->post($id, $data);
                        break;
                    }

                    $action = 'create';
                    $return = $this->create($data);
                    break;
                // PUT
                case 'put':
                    $id   = $this->getIdentifier($routeMatch, $request);
                    $data = $this->processBodyContent($request);

                    if ($id !== false) {
                        $action = 'update';
                        $return = $this->update($id, $data);
                        break;
                    }

                    $action = 'replaceList';
                    $return = $this->replaceList($data);
                    break;
                // All others...
                default:
                    $response = $e->getResponse();
                    $response->setStatusCode(405);
                    return $response;
            }
        } catch (\Exception $ex) {
            /** @var \Zend\Http\PhpEnvironment\Response $response */
            $headers = new Headers();
            $headers->addHeaderLine('Content-Type: application/json');
            $response = $e->getResponse();
            $response->setHeaders($headers);
            $response->setContent(json_encode([
                'detail' => $ex->getMessage(),
                'title' => 'Generic error',
                'status' => 400,
                'type' => 'http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html'
            ]));
            $response->setStatusCode(400);
            return $response;
        }
        $routeMatch->setParam('action', $action);
        $e->setResult($return);
        return $return;
    }

    /**
     * Process post data
     *
     * @param $request
     * @return mixed
     */
    public function processPostData(RequestInterface $request)
    {
        if ($this->requestHasContentType($request, self::CONTENT_TYPE_JSON)) {
            $data = Json::decode($request->getContent(), $this->jsonDecodeType);
        } else {
            $data = $request->getPost()->toArray();
        }
        return $data;
    }


}