<?php

namespace LajiForm\Controller;

use Triplestore\Service\ObjectManager;
use Zend\Cache\Storage\Adapter\Filesystem;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class FlushController extends AbstractActionController
{
    private $formCache;
    private $om;

    public function __construct(Filesystem $formCache, ObjectManager $om)
    {
        $this->formCache = $formCache;
        $this->om = $om;
    }

    public function indexAction()
    {
        $this->formCache->flush();
        $this->om->getCache()->flush();
        return new JsonModel(['flush' => 'ok']);
    }
}
