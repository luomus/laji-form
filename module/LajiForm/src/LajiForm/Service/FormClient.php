<?php

namespace LajiForm\Service;

use LajiStoreClient\Service\LajiStoreClient;
use LForm\Form\InstructionsProvider;
use mikemccabe\JsonPatch\JsonPatch;
use Zend\Cache\Storage\StorageInterface;
use Zend\Log\LoggerInterface;

const DEFAULT_GEOMETYRY_VALIDATOR = [
    "requireShape" => true,
    "maximumSize" => 10,
    "includeGatheringUnits" => true,
    "message" => [
        "missingGeometries" => "@geometryValidation",
        "invalidBoundingBoxHectares"=> "@geometryHectaresMaxValidation",
        "notGeometry" => "@geometryValidation",
        "missingType" => "@geometryValidation",
        "invalidRadius" => "@geometryValidation",
        "invalidCoordinates" => "@geometryValidation",
        "invalidGeometries" => "@geometryValidation",
        "noOverlap" => "@geometryValidation"
    ],
    "boundingBoxMaxHectares" => 5000000
];

const DEFAULT_GEOMETRY_VALIDATOR_TRANSLATIONS = [
    "@geometryValidation" => [
        "en" => "Gathering must have at least one feature.",
        "sv" => "Platsen måste ha åtminstone en figur.",
        "fi" => "Paikalla täytyy olla vähintään yksi kuvio."
    ],
    "@geometryHectaresMaxValidation" => [
        "en" => "Too big area. Maximum is %{max} hectares",
        "sv" => "För stort område. Maximalt är %{max} hektar",
        "fi" => "Liian iso alue. Maksimi on %{max} hehtaaria",
    ]
];

class FormClient implements InstructionsProvider
{
    const CACHE_PREFIX = 'form-instructions-cache-';

    private $prepopulateService;
    private $lajiStoreClient;
    private $logger;
    private $cache;
    private $cycleDetect = [];
    private $triplestoreInterpreter;
    private $supportedLang = [
        'fi' => 'fi',
        'en' => 'en',
        'sv' => 'sv'
    ];

    protected $resultLang;
    protected $noTranslationKeys = [
        'id',
        'type',
        'translations',
        'filter',
        'uiSchema'
    ];

    public function __construct(PrepopulateService $prepopulateService, LajiStoreClient $lajiStoreClient, LoggerInterface $logger, StorageInterface $cache,  TriplestoreInterpreter $triplestoreInterpreter)
    {
        $this->prepopulateService = $prepopulateService;
        $this->lajiStoreClient = $lajiStoreClient;
        $this->logger = $logger;
        $this->cache = $cache;
        $this->triplestoreInterpreter = $triplestoreInterpreter;
    }

    /**
     * @return string
     */
    public function getResultLang()
    {
        return $this->resultLang;
    }

    /**
     * @param $resultLang
     * @throws \Exception
     */
    public function setResultLang($resultLang)
    {
        if (empty($resultLang)) {
            return;
        }
        if (!isset($this->supportedLang[$resultLang])) {
            throw new \Exception("Invalid language given! Got $resultLang when expecting one of "
                . implode(', ', $this->supportedLang));
        }
        $this->resultLang = $resultLang;
    }

    public function prepareInstructions($instructions, $expand = true)
    {
        if ($expand) {
            // add prepopulated data
            if (
                isset($instructions['options']) &&
                isset($instructions['options']['prepopulateWithInformalTaxonGroups']) &&
                is_array($instructions['options']['prepopulateWithInformalTaxonGroups']) &&
                count($instructions['options']['prepopulateWithInformalTaxonGroups']) > 0
            ) {
                $document = isset($instructions['options']['prepopulatedDocument']) ? $instructions['options']['prepopulatedDocument'] : [];
                $instructions['options']['prepopulatedDocument'] = $this->prepopulateService->prepopulateByInformalTaxonGroup(
                    $instructions['options']['prepopulateWithInformalTaxonGroups'],
                    $document
                );
            }

            // add expandable data
            $instructions = $this->expandSpreads($instructions);

            // add linked forms
            if (isset($instructions['baseFormID'])) {
                $subForm = $this->getSubForm($instructions['baseFormID'], $expand, $instructions);
                $instructions = array_replace($subForm, $instructions);
                unset($instructions['baseFormID']);
            }
            if (isset($instructions['fields'])) {
                // add forms that are linked using field
                $fields = [];
                foreach($instructions['fields'] as $field) {
                    if (isset($field['formID']) && !isset($field['type'])) {
                        $subForm = $this->getSubForm($field['formID'], $expand, $instructions);
                        if (isset($subForm['fields'])) {
                            $this->mergeForms($subForm['fields'], $fields);
                        }
                    } else {
                        $fields[] = $field;
                    }
                }
                $instructions['fields'] = $fields;
            }
            if (isset($instructions['patch'])) {
                $instructions = JsonPatch::patch($instructions, $instructions['patch']);
                unset($instructions['patch']);
            }
            $defaultLang = isset($instructions['language']) ? $instructions['language'] : 'en';
            $otherLangs  = $this->supportedLang;
            $this->addDefaultValidators($instructions);
            $this->expandTripleStoreFields($instructions, $defaultLang, $otherLangs, $instructions);
        }
        return $instructions;
    }

    protected function mergeForms($fields, &$mergeTo) {
        $col = array_column($mergeTo, 'name');
        foreach ($fields as $field) {
            $key = array_search($field['name'], $col);
            if ($key !== false) {
                if (isset($field['fields']) && isset($mergeTo[$key]['fields'])) {
                    $this->mergeForms($field['fields'], $mergeTo[$key]['fields']);
                }
            } else {
                array_push($mergeTo, $field);
            }
        }
    }

    protected function translate($instructions) {
        if (!isset($instructions['translations']) || $this->resultLang === null) {
            return $instructions;
        }
        $lang = isset($instructions['supportedLanguage']) &&
                !in_array($this->resultLang, $instructions['supportedLanguage']) ?
            $instructions['supportedLanguage'][0] : $this->resultLang;
        if (isset($instructions['translations'][$lang])) {
            $instructions = $this->_translate(
                $instructions,
                $instructions['translations'][$lang],
                $this->noTranslationKeys
            );
            if (isset($instructions['uiSchema'])) {
                $instructions['uiSchema'] = $this->_translate(
                    $instructions['uiSchema'],
                    $instructions['translations'][$lang]
                );
            }
            $instructions['language'] = $lang;
            unset($instructions['translations']);
        }
        return $instructions;
    }

    protected function _translate($instructions, $translations, $noTranslationKeys = [], $level = 0) {
        foreach($instructions as $key => $value) {
            if (is_string($key) && is_string($value) && in_array($key, $noTranslationKeys)) {
                continue;
            }
            if (is_string($value) && strpos($value, '@') === 0) {
                if (isset($translations[$value])) {
                    $instructions[$key] = $translations[$value];
                } else {
                    $shortValue = substr($value, 1);
                    if (isset($translations[$shortValue])) {
                        $instructions[$key] = $translations[$shortValue];
                    }
                }
            } else if (is_array($value)) {
                $instructions[$key] = $this->_translate($instructions[$key], $translations, $noTranslationKeys, ($level + 1));
            }
        }
        return $instructions;
    }

    protected function expandSpreads($instructions, $level = 0) {
        $isInArray = false;
        foreach($instructions as $key => $value) {
            if (is_string($value) && strpos($value, '...') === 0) {
                if (is_numeric($key)) {
                    $isInArray = true;
                }
                $instructions[$key] = $this->getExpandedValue(substr($value, 3));
            } else if (is_array($value)) {
                $instructions[$key] = $this->expandSpreads($instructions[$key], ($level + 1));
            }
        }
        if ($isInArray) {
            $it = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($instructions));
            $result = [];
            foreach ($it as $item) {
                $result[] = $item;
            }
            return $result;
        }
        return $instructions;
    }

    protected function getExpandedValue($field) {
        $parts = explode(':', $field);
        if (!isset($parts[1])) {
            return [];
        }
        $validValues = explode(',', $parts[1]);
        switch ($parts[0]) {
            case 'taxonSet':
                return $this->prepopulateService->taxonSets($validValues);
            default:
                throw new \Exception('Unrecognised spread field ' . $parts[0]);
        }
    }

    protected function expandTripleStoreFields(&$instructions, $defaultLang, $otherLangs, &$rootInstructions) {
        if (!isset($instructions['fields'])) {
            return;
        }
        foreach($instructions['fields'] as &$field) {
            if (isset($field['fields'])) {
                $this->expandTripleStoreFields($field, $defaultLang, $otherLangs, $rootInstructions);
            }
            if (!isset($field['name']) || !$this->isTriplestoreField($field['name'])) {
                continue;
            }
            $this->triplestoreInterpreter->expand($field, $otherLangs, $rootInstructions);
        }
    }

    protected function isTriplestoreField($name) {
        return strpos($name, '.') !== false || strpos($name, ':') !== false;
    }

    protected function getSubForm($formID, $expand, &$instructions) {
        $subForm = $this->_getForm($formID, $expand);
        if (isset($subForm['translations'])) {
            if (!isset($instructions['translations'])) {
                $instructions['translations'] = [];
            }
            $instructions['translations'] = array_replace_recursive($subForm['translations'], $instructions['translations']);
            unset($subForm['translations']);
        }
        if (isset($subForm['uiSchema'])) {
            if (!isset($instructions['uiSchema'])) {
                $instructions['uiSchema'] = [];
            }
            $instructions['uiSchema'] = array_replace_recursive($subForm['uiSchema'], $instructions['uiSchema']);
            unset($subForm['uiSchema']);
        }
        return $subForm;
    }

    public function getForm($id, $expand = true, $translate = true) {
        $this->cycleDetect = [];
        return $this->_getForm($id, $expand, true, $translate);
    }

    public function getInstructions($id)
    {
        return $this->getForm($id, true);
    }

    private function _getForm($id, $expand = true, $cache = true, $translate = false) {
        $cacheKey = $this->getCacheKey($id, $expand);
        if ($cache && $this->cache->hasItem($cacheKey)) {
            return $translate ? $this->translate($this->cache->getItem($cacheKey)) : $this->cache->getItem($cacheKey);
        }
        if (in_array($id, $this->cycleDetect)) {
            $this->logger->err('Circular form generation! ' . implode(' -> ', $this->cycleDetect) . ' -> ' . $id);
            return [];
        }
        $this->cycleDetect[] = $id;
        $result = $this->lajiStoreClient->sendGet(LajiStoreClient::RESOURCE_FORMS, $id);
        if (!$result->isSuccess()){
            return [];
        }
        $instructions = $this->prepareInstructions($result->getContent(), $expand);
        if ($cache) {
            $this->cache->setItem($cacheKey, $instructions);
        }
        return $translate ? $this->translate($instructions) : $instructions;
    }

    public function deleteForm($id) {
        $this->invalidateCache($id);
        return $this->lajiStoreClient->sendDelete(LajiStoreClient::RESOURCE_FORMS, $id);
    }

    public function getAllForms() {
        $cacheKey = $this->getCacheKey('-ALL-' . $this->resultLang);
        $pickedOptions = [
            'allowExcel',
            'allowTemplate',
            'dataset',
            'emptyOnNoCount',
            'forms',
            'excludeFromGlobalExcel',
            'hasAdmins',
            'prepopulateWithInformalTaxonGroups',
            'restrictAccess',
            'secondaryCopy',
            'sidebarFormLabel',
            'useNamedPlaces',
            'viewerType',
			'disabled',
			'shortTitleFromCollectionName'
        ];
        if (!$this->cache->hasItem($cacheKey)) {
            $forms = $this->lajiStoreClient->sendGet(LajiStoreClient::RESOURCE_FORMS, null, ['page_size' => 100, 'page' => 1]);
            $result = [
                'forms' => [],
                'translations' => []
            ];
            foreach($forms as $formLD) {
                $id = $formLD['id'];
                $form = $this->_getForm($id, true, true, true);
                $options = [];
                if (isset($form['options'])) {
                    foreach ($pickedOptions as $option) {
                        if (isset($form['options'][$option])) {
                            $options[$option] = $form['options'][$option];
                        }
                    }
                }
                $logo = isset($form['logo']) ? $form['logo'] : '';
                $title = isset($form['title']) ? $form['title'] : '';
                $category = isset($form['category']) ? $form['category'] : '';
                $collectionID = isset($form['collectionID']) ? $form['collectionID'] : '';
                $description = isset($form['description']) ? $form['description'] : '';
                $shortDescription = isset($form['shortDescription']) ? $form['shortDescription'] : '';
                $data = [
                    'id' => $id,
                    'logo' => $logo,
                    'title' => $title,
                    'description' => $description,
                    'shortDescription' => $shortDescription,
                    'supportedLanguage' => ['en','fi','sv'],
                    'category' => $category,
                    'collectionID' => $collectionID,
                ];
                if (count($options) > 0) {
                    $data['options'] = $options;
                }
                $result['forms'][] = $data;
            }
            $this->cache->setItem($cacheKey, $result);
        }
        return $this->cache->getItem($cacheKey);
    }

    public function saveForm($data, $id = null) {
        if (empty($data)) {
            throw new \ErrorException('Missing form data!');
        }
        $this->prepareData($data);
        if ($id !== null) {
            $this->invalidateCache($id);
            return $this->lajiStoreClient->sendPut(LajiStoreClient::RESOURCE_FORMS, $id, [], $data);
        } else {
            return $this->lajiStoreClient->sendPost(LajiStoreClient::RESOURCE_FORMS, $id, [], $data);
        }
    }

    protected function addDefaultValidators(&$instructions) {
        if (!isset($instructions['fields'])) {
            return;
        }
        $hasDefaultGeometryValidator = false;
        foreach($instructions['fields'] as &$field) {
            if (isset($field['name']) && $field['name'] === 'gatherings' && isset($field['fields'])) {
                foreach ($field['fields'] as &$gatheringField) {
                    if (isset($gatheringField['name']) && ($gatheringField['name'] === 'MZ.geometry' || $gatheringField['name'] === 'MY.geometry')) {
                        if (!isset($gatheringField['validators'])) {
                            $gatheringField['validators'] = [];
                        }
                        if (!array_key_exists('geometry', $gatheringField['validators'])) {
                            $gatheringField['validators']['geometry'] = DEFAULT_GEOMETYRY_VALIDATOR;
                            $hasDefaultGeometryValidator = true;
                        } else if ($gatheringField['validators']['geometry'] === false) {
                            unset($gatheringField['validators']['geometry']);
                            if (count($gatheringField['validators']) === 0) {
                                unset($gatheringField['validators']);
                            }
                        }
                        break;
                    }
                }
                break;
            }
        }
        if ($hasDefaultGeometryValidator) {
            if (!isset($instructions['translations'])) {
                $instructions['translations'] = ['en' => [], 'sv' => [], 'fi' => []];
            }
            foreach($instructions['translations'] as $lang => $translations) {
                foreach (DEFAULT_GEOMETRY_VALIDATOR_TRANSLATIONS as $place => $data) {
                    if (isset($translations[$place])) {
                        continue;
                    }
                    $instructions['translations'][$lang][$place] = $data[$lang];
                }
            }
        }
    }

    protected function prepareData(&$data) {
        if (isset($data['template'])) {
            $this->prepareETag($data['template']);
        }
    }

    protected function prepareETag(&$template) {
        if (isset($template['ETag'])) {
            unset($template['ETag']);
        }
        $content = json_encode($template);
        if ($content) {
            $template['ETag'] = md5($content);
        }
    }

    protected function invalidateCache($id) {
        $cacheKeys = [
            $this->getCacheKey('-ALL'),
            $this->getCacheKey($id, true),
            $this->getCacheKey($id, false),
        ];
        foreach($this->supportedLang as $lang) {
            $this->getCacheKey('-ALL', true, $lang);
            $cacheKeys[] = $this->getCacheKey($id, true, $lang);
            $cacheKeys[] = $this->getCacheKey($id, false, $lang);
        }
        $this->cache->removeItems($cacheKeys);
    }

    protected function getCacheKey($id, $expand = true, $lang = null) {
        return self::CACHE_PREFIX . str_replace('.','_', $id) . '-' . $expand . $lang;
    }

    protected function addTranslation($value, $sourceTranslations, &$targetTranslations) {
        foreach($sourceTranslations as $lang => $translation) {
            if (isset($translation[$value])) {
                if (!isset($targetTranslations[$lang])) {
                    $targetTranslations[$lang] = [];
                }
                $targetTranslations[$lang][$value] = $translation[$value];
            }
        }
    }

}
