<?php

namespace LajiForm\Service;


use Common\Service\IdService;
use Triplestore\Service\MetadataService;

class TriplestoreInterpreter
{
    protected $metadataService;
    protected $propertiesService;

    public function __construct(MetadataService $metadataService)
    {
        $this->metadataService = $metadataService;
        $this->propertiesService = $metadataService->getAllProperties();
    }

    public function expand(&$spec, $langs, &$insturecitons) {
        $field = $spec['name'];
        if (!$field === 'MY.geometry' && !$this->propertiesService->hasProperty($field)) {
            throw new \Exception('Cannot find data for the field ' . $field);
        }
        if (!isset($spec['type'])) {
            $type = $this->propertiesService->getType($field);
            if ($this->metadataService->altExists($type)) {
                if ($this->metadataService->hasExtra($type)) {
                    $insturecitons['extra'][IdService::getDotLess($field)] = $this->metadataService->getExtra($type);
                }

                $spec['type'] = 'select';
                $alt = $this->metadataService->getAlt($type);
                $options = [];
                if (!$this->propertiesService->isRequired($field)) {
                    $options[''] = '';
                }
                $roots = [];
                foreach($alt as $key => $labels) {
                    $options[$key] = $key;
                    if ($this->hasExtraParent($insturecitons, $field)
                        && !isset($insturecitons['extra'][IdService::getDotLess($field)]['altParent'][$key])) {
                         $roots[$key] = [];
                    }
                    foreach($langs as $otherLang) {
                        if (!isset($insturecitons['translations'][$otherLang][$options[$key]])) {
                            $insturecitons['translations'][$otherLang][$options[$key]] =
                                isset($labels[$otherLang]) ? $labels[$otherLang] : $key;
                        }
                    }
                    $options[$key] = '@' . $options[$key];
                }
                if (!empty($roots) && $this->hasExtraParent($insturecitons, $field)) {
                    $insturecitons['extra'][IdService::getDotLess($field)]['altParent']
                        = array_merge($roots, $insturecitons['extra'][IdService::getDotLess($field)]['altParent']);
                }
                $spec['options']['value_options'] = $this->filterAlt($spec, $options);
            } else {
                $spec['type'] = $this->getType($type);
            }
        }
        if (!array_key_exists('required', $spec) && $this->propertiesService->isRequired($field)) {
            $spec['required'] = true;
        }
        if (!isset($spec['label'])) {
            $spec['label'] = $field;
            foreach($langs as $otherLang) {
                if (!isset($insturecitons['translations'][$otherLang][$spec['label']])) {
                    $insturecitons['translations'][$otherLang][$spec['label']] =
                        $this->propertiesService->getLabel($field, $otherLang);
                }
            }
            $spec['label'] = '@' . $spec['label'];
        }
        if ($this->propertiesService->hasMany($field) && $spec['type'] !== 'collection') {
            $spec['options']['target_element'] = ['type' => $spec['type']];
            $spec['type'] = 'collection';
        }

        $spec['name'] = IdService::getDotLess($field);
    }

    protected function filterAlt(&$spec, $options) {
        if (!isset($spec['options'])) {
            return $options;
        }
        if (isset($spec['options']['whitelist'])) {
            $options = array_intersect_key($options, array_flip($spec['options']['whitelist']));
            unset($spec['options']['whitelist']);
        }
        if (isset($spec['options']['blacklist'])) {
            $options = array_diff_key($options, array_flip($spec['options']['blacklist']));
            unset($spec['options']['blacklist']);
        }
        return $options;
    }

    protected function getType($type) {
        switch($type) {
            case 'xsd:time':
                return 'time';
            case 'xsd:dateTime':
                return 'datetime';
            case 'xsd:date':
                return 'date';
            case 'xsd:boolean':
                return 'checkbox';
            case 'xsd:integer':
                return 'integer';
            case 'xsd:nonNegativeInteger':
                return 'integer:nonNegativeInteger';
            case 'xsd:positiveInteger':
                return 'integer:positiveInteger';
            case 'xsd:decimal':
                return 'number';
            case 'MZ.keyAny':
                return 'anyObject';
            case 'MZ.keyValue':
                return 'keyValue';
        }
        return 'text';
    }

    protected function hasExtraParent($instructions, $field) {
        return
           isset($instructions)
        && isset($instructions['extra'])
        && isset($instructions['extra'][IdService::getDotLess($field)])
        && isset($instructions['extra'][IdService::getDotLess($field)]['altParent']);
    }

}