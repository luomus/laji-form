<?php

namespace LajiForm\Service;

use Common\Service\IdService;
use LajiStoreClient\Service\LajiStoreClient;

class PrepopulateService
{
    protected $lajiApiClient;

    public function __construct(LajiStoreClient $lajiApiClient)
    {
        $this->lajiApiClient = $lajiApiClient;
    }

    public function taxonSets($sets) {
        $result = $this->lajiApiClient->sendGet('taxa', null, [
            'taxonSets' => join(',', $sets),
            'selectedFields' => 'id',
            'pageSize'  => 1000
        ]);
        $result = $result->getContent();
        if (!count($result['results'])) {
            return [];
        }
        $ids = [];
        foreach ($result['results'] as $row) {
            $ids[] = IdService::getQName($row['id'], true);
        }
        return $ids;
    }

    public function prepopulateByInformalTaxonGroup($gourps, $template) {
        $result = $this->lajiApiClient->sendGet('taxa/MX.37600/species', null, [
            'informalGroupFilters' => join(',', $gourps),
            'selectedFields' => 'id,scientificName,vernacularName',
            'lang' => 'fi',
            'taxonRanks' => 'MX.species',
            'onlyFinnish' => 'true',
            'pageSize'  => 1000
        ]);
        $result = $result->getContent();
        if (!count($result['results'])) {
            return $template;
        }
        $units = [];
        foreach ($result['results'] as $row) {
            $units[] = [
                'identifications' => [[
                    'taxonID' => IdService::getQName($row['id'], true),
                    'taxonVerbatim' => isset( $row['vernacularName']) ? $row['vernacularName'] : '',
                    'taxon' => isset( $row['scientificName']) ? $row['scientificName'] : ''
                ]]
            ];
        }
        $popDoc = [
            'gatherings' => [
                [
                    'units' => $units
                ]
            ]
        ];
        return $this->prepopulateDocument($template, $popDoc);
    }

    private function prepopulateDocument($template, $popDocument) {
        return $this->mergeLevel(
            [
                'gatheringsEvent' => null,
                'gatherings' => [
                    'units' => [
                        'identifications' => null,
                        'unitGathering' => null,
                        'typeSpecimen' => null
                    ]
                ]
            ],
            $template,
            $popDocument
        );
    }

    private function mergeLevel($keys, $template, $pop) {
        foreach ($keys as $key => $value) {
            if (!empty($pop[$key])) {
                if (empty($template[$key])) {
                    $template[$key] = $pop[$key];
                } else {
                    foreach ($pop[$key] as $subKey => $subPop) {
                        if ($value === null) {
                            break 2;
                        } else {
                            $template[$key][$subKey] = $this->mergeLevel(
                                $value,
                                isset($template[$key][$subKey]) ?
                                    $template[$key][$subKey] :
                                    $template[$key][0],
                                $subPop
                            );
                        }
                    }
                }
            }
            unset($pop[$key]);
        }
        return array_replace_recursive($template, $pop);
    }
}