<?php
namespace LajiForm\Factory;


use LajiStoreClient\Service\LajiStoreClient;
use Zend\Http\Client;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LajiApiClientFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return LajiStoreClient
     * @throws \Exception
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \LajiForm\Options\LajiApiOptions $options */
        $options = $serviceLocator->get('LajiForm\Options\LajiApiOptions');
        $client = new Client(null, $options->getAdapterOptions());
        $client->setAuth($options->getAccessToken(), null);
        $headers = $client->getRequest()->getHeaders();
        $headers->addHeaderLine('Authorization: ' . $options->getAccessToken());

        return new LajiStoreClient($options->getUrl(), $client);
    }
}