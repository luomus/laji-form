<?php

namespace LajiForm\Factory;


use LajiForm\Exceptions\MissingConfigurationException;
use LajiForm\Options\LajiApiOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LajiApiOptionsFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator);
        $config  = new LajiApiOptions($options);

        return $config;
    }

    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws MissingConfigurationException
     */
    public function getOptions(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['laji-api']) ? $options['laji-api'] : null;

        if (null === $options) {
            throw new MissingConfigurationException(
                'Configuration for lajiApi could not be found in "laji-api".'
            );
        }

        return $options;
    }
}