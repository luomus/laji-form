<?php
namespace LajiForm\Factory;

use LajiForm\Controller\FlushController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FlushControllerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return FlushController
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $parentServiceLocator = $serviceLocator->getServiceLocator();
        return new FlushController(
            $parentServiceLocator->get('cache'),
            $parentServiceLocator->get('Triplestore\ObjectManager')
        );
    }
}