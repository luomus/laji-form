<?php
namespace LajiForm\Factory;


use LajiStoreClient\Service\LajiStoreClient;
use Zend\Http\Client;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LajiStoreClientFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \LajiForm\Options\LajiStoreOptions $options */
        $options = $serviceLocator->get('LajiForm\Options\LajiStoreOptions');
        $client = new Client(null, $options->getAdapterOptions());
        $client->setAuth($options->getUsername(), $options->getPassword());

        return new LajiStoreClient($options->getUrl(), $client);
    }
}