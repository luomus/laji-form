<?php

namespace LajiForm\Factory;


use LajiForm\Service\PrepopulateService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PrepopulateServiceFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return PrepopulateService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new PrepopulateService(
            $serviceLocator->get('LajiForm\Service\LajiApiClient')
        );
    }
}