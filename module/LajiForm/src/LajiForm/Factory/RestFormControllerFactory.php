<?php
namespace LajiForm\Factory;


use LajiForm\Controller\RestFormController;
use LajiForm\Service\FormClient;
use LForm\Form\FormElementManager;
use LForm\Form\InstructionsFactory;
use LForm\Form\View\HelperConfig;
use LForm\Service\FormService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\View\HelperPluginManager;
use Zend\View\Renderer\PhpRenderer;

class RestFormControllerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {

        /** @var \Zend\ServiceManager\ServiceManager $parentLocator */
        $parentLocator = $serviceLocator->getServiceLocator();
        /** @var FormClient $formClient */
        $formClient = $parentLocator->get('LajiForm\Service\FormClient');

        $elementManager = new FormElementManager();
        $elementManager->setServiceLocator($parentLocator);
        $factory = new InstructionsFactory($elementManager);
        $phpRenderer = new PhpRenderer();
        $phpRenderer->setHelperPluginManager(new HelperPluginManager(new HelperConfig()));
        $formService = new FormService($formClient, $factory, $phpRenderer);

        $parentLocator->setAllowOverride(true);
        $parentLocator->setInvokableClass('FormElementManager', $elementManager);

        return new RestFormController(
            $formClient,
            $formService,
            $parentLocator->get('ViewRenderer')
        );
    }
}