<?php

namespace LajiForm\Factory;


use LajiForm\Exceptions\MissingConfigurationException;
use LajiForm\Options\LajiStoreOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LajiStoreOptionsFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator);
        $config  = new LajiStoreOptions($options);

        return $config;
    }

    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws MissingConfigurationException
     */
    public function getOptions(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['laji-store']) ? $options['laji-store'] : null;

        if (null === $options) {
            throw new MissingConfigurationException(
                'Configuration for lajiStore could not be found in "laji-store".'
            );
        }

        return $options;
    }
}