<?php
namespace LajiForm\Factory;

use LajiForm\Controller\DemoController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class DemoControllerFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return new DemoController(
            $serviceLocator->getServiceLocator()->get('LajiForm\Service\LajiStoreClient')
        );
    }
}