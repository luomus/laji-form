<?php
namespace LajiForm\Factory;


use LajiForm\Service\FormClient;
use LajiForm\Service\TriplestoreInterpreter;
use LajiStoreClient\Service\LajiStoreClient;
use Zend\Http\Client;
use Zend\I18n\Translator\Translator;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class FormClientFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Triplestore\Service\ObjectManager $om */
        $om = $serviceLocator->get('TripleStore\Objectmanager');
        return new FormClient(
            $serviceLocator->get('LajiForm\Service\PrepopulateService'),
            $serviceLocator->get('LajiForm\Service\LajiStoreClient'),
            $serviceLocator->get('logger'),
            $serviceLocator->get('cache'),
            new TriplestoreInterpreter($om->getMetadataService())
        );
    }
}