<?php

namespace LajiForm\Factory;

use LajiForm\Options\LogOptions;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LogOptionsFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['log']) ? $options['log'] : [];
        $config  = new LogOptions($options);

        return $config;
    }
}