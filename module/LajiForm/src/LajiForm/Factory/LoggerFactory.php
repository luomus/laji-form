<?php

namespace LajiForm\Factory;


use LajiForm\Options\LogOptions;
use Zend\Log\Filter\Priority;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoggerFactory implements FactoryInterface
{
    /**
     * Creates the service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return Logger
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var LogOptions $logOptions */
        $logOptions = $serviceLocator->get('LajiForm\Options\LogOptions');
        $logFile = sprintf($logOptions->getFile(), date($logOptions->getPattern()));
        $logger = new Logger();
        $writer = new Stream($logFile);

        $logger->addWriter($writer);
        $writer->addFilter(new Priority($logOptions->getLevel()));

        return $logger;
    }
}