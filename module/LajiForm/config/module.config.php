<?php
namespace LajiForm;

return array(
    'router' => array(
        'routes' => array(
            'laji-form' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/lajiform[/:id]',
                    'constraints' => array(
                        'id'     => '[a-zA-Z\.0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'LajiForm\Controller\RestForm'
                    ),
                ),
            ),
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'LajiForm\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/lajiform/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'LajiForm\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'LajiForm\Options\LajiStoreOptions' => Factory\LajiStoreOptionsFactory::class,
            'LajiForm\Options\LajiApiOptions' => Factory\LajiApiOptionsFactory::class,
            'LajiForm\Options\LogOptions' => Factory\LogOptionsFactory::class,
            'LajiForm\Service\LajiStoreClient' => Factory\LajiStoreClientFactory::class,
            'LajiForm\Service\LajiApiClient' => Factory\LajiApiClientFactory::class,
            'LajiForm\Service\PrepopulateService' => Factory\PrepopulateServiceFactory::class,
            'LajiForm\Service\FormClient' => Factory\FormClientFactory::class,
            //'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
            'cache' => 'Zend\Cache\Service\StorageCacheFactory',
            'logger' => Factory\LoggerFactory::class,
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'LajiForm\Controller\RestForm' => Factory\RestFormControllerFactory::class,
            'LajiForm\Controller\Demo' => Factory\DemoControllerFactory::class,
            'LajiForm\Controller\Flush' => Factory\FlushControllerFactory::class,
        ),
        'invokables' => array(
            'LajiForm\Controller\Index' => Controller\IndexController::class
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
