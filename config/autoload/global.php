<?php
return array(
    'laji-store' => [
        'url' => getenv('LAJISTORE_URL'),
        'username' => getenv('LAJISTORE_USERNAME'),
        'password' => getenv('LAJISTORE_PASSWORD'),
    ],
    'laji-api' => [
        'url' => getenv('LAJIAPI_URL'),
        'access_token' => getenv('LAJIAPI_TOKEN'),
    ]
);
