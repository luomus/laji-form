<?php
return array(
    'cache' => array(
        'adapter' => array(
            'name' => 'filesystem',
            'options' => array (
                'cache_dir' => 'data/cache/forms',
                'ttl'       => 43200,
            )
        ),
        'plugins' => array(
            'exception_handler' => array(
                'throw_exceptions' => false
            ),
            'serializer' => array (
                'serializer' => 'Zend\Serializer\Adapter\PhpSerialize',
            )
        )
    )
);