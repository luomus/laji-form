# base image
FROM php:7.1-fpm

ENV ORACLE_INSTANTCLIENT=https://cdn.laji.fi/_inst/
ENV LD_LIBRARY_PATH=/usr/local/instantclient
ENV ORACLE_HOME=/usr/local/instantclient

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# install dependencies
RUN apt-get update \
    && apt-get install -y libpng-dev libturbojpeg-dev libwebp-dev libxpm-dev libldap2-dev \
       zlib1g-dev libzip-dev libicu-dev libaio-dev g++ git libxml2-dev \
       wget unzip \
    && mkdir -p /usr/lib/x86_64-linux-gnu \
    && mv ${PHP_INI_DIR}/php.ini-development ${PHP_INI_DIR}/php.ini \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
    && docker-php-ext-install ldap \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && docker-php-ext-configure gd \
    && docker-php-ext-install gd \
    && docker-php-ext-configure soap \
    && docker-php-ext-install soap \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install zip \
    && pecl install xdebug-2.6.0 \
    && docker-php-ext-enable xdebug \
    && wget ${ORACLE_INSTANTCLIENT}instantclient-basic-linux.x64-12.2.0.1.0.zip -P /tmp/ \
    && wget ${ORACLE_INSTANTCLIENT}instantclient-sdk-linux.x64-12.2.0.1.0.zip -P /tmp/ \
    && unzip /tmp/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /usr/local/ \
    && unzip /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /usr/local/ \
    && ln -s /usr/local/instantclient_12_2 /usr/local/instantclient \
    && ln -s /usr/local/instantclient/libclntsh.so.12.1 /usr/local/instantclient/libclntsh.so \
    && echo 'instantclient,/usr/local/instantclient' | pecl install oci8-2.2.0 \
    && apt-get remove -y wget unzip \
    && rm -rf /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip \
    && rm -rf /tmp/instantclient-sdk-linux.x64-12.2.0.1.0.zip \
    && rm -rf /var/lib/apt/lists/*

# configure dependencies
RUN echo "error_reporting = E_ALL" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "display_startup_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "display_errors = On" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_autostart = 1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.idekey = PHPSTORM" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_enable = 1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_connect_back = 1" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_port = 9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_handler = dbgp" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.remote_mode = req" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "opcache.enable=1" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.validate_timestamps=1" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.max_accelerated_files=10000" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.memory_consumption=192" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.max_wasted_percentage=10" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.interned_strings_buffer=16" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "opcache.fast_shutdown=1" >> /usr/local/etc/php/conf.d/opcache.ini \
    && echo "memory_limit = 4096M" >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini \
    && echo "extension=oci8.so" > /usr/local/etc/php/conf.d/php-oci8.ini

WORKDIR /var/www/html

COPY ./composer* ./
RUN php composer.phar install --no-scripts --ansi --no-interaction

# add app
COPY --chown=www-data:www-data . .

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["php-fpm"]